
from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(max_length=15, min_length=5)
    overview: str = Field(max_length=100, min_length=15)
    year: int  = Field(le=2023, ge=1900)
    rating: float = Field(le=10.0, ge=0.0)
    category: str = Field(max_length=15, min_length=3)

    class Config:
        schema_extra = {
            "example" : {   
                "id": 1,
                "title": "Mi peli",
                "overview": "Mi resumen de película",
                "year": 2023,
                "rating": 0,
                "category": "string"
            }
        }

